# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        Наименование  модуля
# Purpose:     Выбрать все панели
#              Заполнить список
#              Пересоздать
#
# Author:       Александр Драгункин --<alexandr69@gmail.com>
#
# Created:     13.07.2018
#
# Copyright:
# Licence:
#-------------------------------------------------------------------------------
try:
    import wingdbstub
except:
    pass

import k3

def refresh():
    if k3.isattrdef("hatchDec")>0:
        k3.delete(k3.k_partly, k3.k_attribute, 'isassign("hatchDec")',k3.k_done)
    try:
        # включаем режим перерегистрации зарегистрированных полилайнов и обработок
        # объектов универсальной панели.
        k3.switch(k3.k_reregister , k3.k_on)
        k3.mbpanel(k3.k_recreate,k3.k_all,k3.k_done)
    except:
        k3.putmsg("не смогли освежиться! расставьте крепеж и сверловку ручками. ",0)
    finally:
        # выключаем режим перерегистрации зарегистрированных полилайнов и обработок объек
        # тов универсальной панели.
        k3.switch(k3.k_reregister , k3.k_off)

    k3.fixing(k3.k_create,k3.k_all,k3.k_done)
    k3.holes(k3.k_create,k3.k_all,k3.k_done)

if __name__ == '__main__':
    refresh()