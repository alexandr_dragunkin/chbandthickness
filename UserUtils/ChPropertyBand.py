# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:
# Purpose:     Замена свойств кромки из Dept в Thickness и обратно
#
# Author:      Александр Драгункин --<alexandr69@gmail.com>
#
# Created:     12.07.2018
#
# Copyright:
# Licence:
#-------------------------------------------------------------------------------

import k3

try:
    import wingdbstub
except:
    pass


from dbaccessforK3 import (RecordSet_ACCESS,
                           DataBase_ACCESS, )

from refresh_panels import refresh


def find_all_band():
    """Находит все кромки в заказе"""
    from DrawingSupp import creator_kromznak

    if 'KROMZNAK' in globals().keys():
        del(KROMZNAK)

    KROMZNAK = creator_kromznak()
    return KROMZNAK

def ch_prop():
    """список ID"""
    global nbase
    nbase = DataBase_ACCESS()
    nbase.adbCon(2)
    lst_id = getNSubstitutions_ID(nbase)
    return lst_id

def getNSubstitutions_ID(nbase):
    """Запрос списка ID подстановок
    для свойства номенклатуры BandType,
    котрые содержат свойства Thickness и Dept """

    sql_str = ["""SELECT NSubstitutions.ID FROM NSubstitutions
    WHERE PropertyID=100 ORDER BY ID;"""]
    rs = nbase.RecordSetOpen(sql_str)
    lrs = []
    if rs.count > 0:
        k3.adbmovefirst(rs.idRS)
        while k3.adbiseof(rs.idRS) == 0:
            lrs.append(int(k3.adbgetvalue(rs.idRS,0,'0')))
            k3.adbmovenext(rs.idRS)
    return lrs

def getThicnessAndDept(nbase, id_):
    """Возвращает словарь свойств для NSubstitutions.ID=id_
    >>> nbase = DataBase_ACCESS()
    ... nbase.adbCon(2)
    ... getThicnessAndDept(nbase, 1144)

    {'Dept': {'val': 2.0, 'id': 1127}, 'Thickness': {'val': 0.0, 'id': 256}}

    """

    sql_str = ["""SELECT NSubstitutions.ItemName, NProperties.Ident, NOrderedPropValues.DValue, NOrderedPropValues.ID
    FROM NSubstitutions
         INNER JOIN (NProperties
             INNER JOIN (NOrderedProp INNER JOIN NOrderedPropValues ON NOrderedProp.ID = NOrderedPropValues.OrderedID) ON NProperties.ID = NOrderedProp.PropertyID)
         ON NSubstitutions.ID = NOrderedPropValues.SubstID
    WHERE (((NSubstitutions.ID)={0}) AND ((NProperties.Ident)='Thickness') AND ((NOrderedProp.CortegeID)=100)) OR (((NOrderedPropValues.DValue)>0) AND ((NSubstitutions.ID)={0}) AND ((NProperties.Ident)='Dept') AND ((NOrderedProp.CortegeID)=100))
    ORDER BY NSubstitutions.ID, NProperties.Ident;

    """]
    res = {}
    rs = nbase.RecordSetOpen(sql_str[0].format(id_))
    if rs.count > 0:
        k3.adbmovefirst(rs.idRS)
        while k3.adbiseof(rs.idRS) == 0:
            key=k3.adbgetvalue(rs.idRS, 'Ident','0')
            val = k3.adbgetvalue(rs.idRS, 'DValue','0')
            idp = k3.adbgetvalue(rs.idRS, 'ID','0')
            res[key] = {'val': val, 'id': int(idp)}
            k3.adbmovenext(rs.idRS)
        rs.Close()
    return res

def Dept2Thickness(nbase, id_, dict_property):
    """Заменяет значение свойства Thickness значением из свойства Dept на основе
    словаря dict_property, который имеет вид схожий со следующим:

    {'Dept': {'val': 2.0, 'id': 1127}, 'Thickness': {'val': 0.0, 'id': 256}}
    """
    sql_str = [""" UPDATE NOrderedPropValues
    SET DValue = {0}
    WHERE ID = {1}
    """]
    if 'Dept' in dict_property.keys():
        rs = nbase.RecordSetModify( sql_str[0].format(dict_property['Dept']['val'],
                                                      dict_property['Thickness']['id']))
    return

def Zero2Thickness(nbase, id_, dict_property):
    """Откат Dept2Thickness.
    Заменяет значение свойства Thickness значением равным 0 на основе
    словаря dict_property, который имеет вид схожий со следующим:

    {'Dept': {'val': 2.0, 'id': 1127}, 'Thickness': {'val': 0.0, 'id': 256}}
    """
    sql_str = [""" UPDATE NOrderedPropValues
    SET DValue = {0}
    WHERE ID = {1}
    """]
    if 'Dept' in dict_property.keys():
        rs = nbase.RecordSetModify( sql_str[0].format(0,
                                                      dict_property['Thickness']['id']))
    return

def refresh_bands_Th():
    """Освежает панели в сцене все кромки панелей станут толстыми, реальной толщины.
    """

    # Список ID подстановок для свойства номенклатуры BandType которые содержат свойства Thickness и Dept
    lst_id = ch_prop()

    # Изменим значение свойства из Dept в Thickness
    global nbase
    for id_ in lst_id:
        dict_property = getThicnessAndDept(nbase, id_)
        Dept2Thickness(nbase, id_, dict_property)

    # Прергружаем справочники
    k3.nprefresh()

    # Освежаем панели в сцене все кромки панелей станут толстыми, реальной толщины
    refresh()

    return

def refresh_bands_ThZero():
    """Освежает панели в сцене все кромки панелей станут толстыми, реальной толщины.
    """

    # Список ID подстановок для свойства номенклатуры BandType которые содержат свойства Thickness и Dept
    lst_id = ch_prop()

    # Изменим значение свойства из Dept в Thickness
    global nbase
    for id_ in lst_id:
        dict_property = getThicnessAndDept(nbase, id_)
        Zero2Thickness(nbase, id_, dict_property)

    # Прергружаем справочники
    k3.nprefresh()

    # Освежаем панели в сцене все кромки панелей станут толстыми, реальной толщины
    refresh()

    return

# ----------------------------------- TESTS ------------------------------------
def test_band_scene():
    b = find_all_band()
    print(b.BandScene)
    for bnd in b.BandScene.keys():
        print(k3.priceinfo(bnd[0], 'matname', 'ggg'))

def test_ch_prop():
    lst_id = ch_prop()
    print(lst_id)
    return lst_id

def test_getThicnessAndDept(lst_id):
    global nbase
    for id_ in lst_id:
        d = getThicnessAndDept(nbase, id_)
        print(d)

def test_Dept2Thickness(lst_id):
    global nbase
    for id_ in lst_id:
        dict_property = getThicnessAndDept(nbase, id_)
        Dept2Thickness(nbase, id_, dict_property)
    k3.nprefresh()
    return

def test_Zero2Thickness(lst_id):
    global nbase
    for id_ in lst_id:
        dict_property = getThicnessAndDept(nbase, id_)
        Zero2Thickness(nbase, id_, dict_property)
    k3.nprefresh()
    return
# ------------------------------------------------------------------------------

def main():
    pass

if __name__=='__main__':
    # получаем информацию по всем кромкам в сцене
    test_band_scene()

    # Список ID подстановок для свойства номенклатуры BandType которые содержат свойства Thickness и Dept
    lst_id = test_ch_prop()

    # Выведем на печать словарь по каждой кромке в номенклатуре
    #  {'Dept': {'val': 2.0, 'id': 1127}, 'Thickness': {'val': 0.0, 'id': 256}}
    test_getThicnessAndDept(lst_id)

    # Изменим значение свойства из Dept в Thickness
    test_Dept2Thickness(lst_id)

    # Освежаем панели в сцене все кромки панелей станут толстыми, реальной толщины
    refresh()

    # Остановимся, что бы посмотреть на толстые кромки (они были тонкие, 0-й толщины)
    k3.interact()

    test_Zero2Thickness(lst_id)

    # Освежаем панели в сцене все кромки у панелей станут нулевой толщины
    refresh()
